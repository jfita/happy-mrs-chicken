class Score {
  var eggs = 0;
  var text: h2d.Text;

  public function new(atlas: h2d.Tile, parent: h2d.Object) {
    final background = new h2d.Bitmap(atlas.sub(2, 365, 182, 81), parent);
    background.setPosition(1371 / 2 - 182 / 2, 30);

    final font = hxd.Res.font.toFont();
    text = new h2d.Text(font, background);
    text.textAlign = Center;
    text.maxWidth = 182;
    text.setPosition(0, 81 / 2 - 50 / 2);
    updateText();
  }

  function updateText() {
    text.text = StringTools.lpad(Std.string(eggs), "0", 3);
  }

  public function newEgg() {
    if (eggs < 999) {
      eggs++;
      updateText();
    }
  } 
}


class MrsChicken extends h2d.SpriteBatch.BatchElement {
  final walk: Array<h2d.Tile>;
  final jump: Array<h2d.Tile>;
  final sound = hxd.Res.mrschicken;
  var walking = true;
  var frame = 0;
  var frameTime = 0.25;
  final rand = new hxd.Rand(Std.random(0x7FFFFFFF));

  public function new(atlas: h2d.Tile) {
    walk = [
	    atlas.sub(0, 0, 150, 157),
	    atlas.sub(151, 0, 150, 157),
	    atlas.sub(0, 0, 150, 157),
	    atlas.sub(302, 0, 150, 157),
	    ];
    jump = [
	    atlas.sub(0, 157, 150, 157),
	    atlas.sub(150, 157, 149, 157),
	    ];
    super(walk[frame]);
    x = 1371 / 2 - 150 / 2;
    y = 771 / 2 - 157 / 2;

    hxd.Window.getInstance().addEventTarget(event -> {
	if (event.kind == EKeyDown) {
	  this.layEgg();
	}
      });
  }

  public override function update(et:Float) {
    frameTime -= et;
    if (frameTime <= 0) {
      frame++;
      if (walking) {
	if (frame >= walk.length) frame = 0;
	this.t = walk[frame];
      } else {
	if (frame >= jump.length) {
	  frame = 0;
	  walking = true;
	  this.t = walk[frame];
	  this.x = 150 + rand.random(1371 - 300);
	  this.y = 157 + rand.random(771 - 314);
	  sound.play();
	} else {
	  onEggLaid(this.x + 150 / 2, this.y + 157);
	  frameTime -= 0.10;
	  this.y -= 80;
	  this.t = jump[frame];
	}
      }
      frameTime += 0.25;
    }
    return super.update(et);
  }

  public function layEgg() {
    if (walking) {
      layAndJump();
    }
  }

  function layAndJump() {
    walking = false;
    frame = 0;
    this.t = jump[frame];
    frameTime = 0.15;
  }

  public dynamic function onEggLaid(x: Float, y: Float) {
  }
}


class Egg extends h2d.SpriteBatch.BatchElement {
  final anim: Array<h2d.Tile>;
  var time = 0.0;
  var hatched = false;
  final targetX: Float;
  final targetY: Float;

  public function new(x: Float, y: Float, atlas: h2d.Tile) {
    anim = [
	    atlas.sub(299, 234, 63, 80),
	    atlas.sub(362, 234, 63, 80),
	    atlas.sub(425, 234, 63, 80),
	    ];
    super(anim[0]);
    targetX = x;
    targetY = y;
    scale = 0.25;
    updatePos();
  }

  function updatePos() {
    this.x = targetX - (63 / 2) * scaleX;
    this.y = targetY - 80 * scaleY;
  }

  public dynamic function onNewChick(x, y: Float) {
  }

  public override function update(et:Float) {
    time += et;
    if (time >  2.30) {
      return false;
    } else if (time > 2) {
      if (!hatched) {
	onNewChick(targetX, targetY);
	hatched = true;
      }
      this.t = anim[2];
    } else if (time > 0.75) {
      this.t = anim[1];
    } else if (time > 0.15) {
      scale = 1;
      updatePos();
    } else {
      scale = 1.05 * time / 0.15;
      updatePos();
    }
    return super.update(et);
  }
}


class Chick extends h2d.SpriteBatch.BatchElement {
  final walk: Array<h2d.Tile>;
  var frame = 0;
  var frameTime = 0.15;
  var played = false;
  final dir: Float;

  public function new(x: Float, y: Float, atlas: h2d.Tile) {
    walk = [
	    atlas.sub(0, 0, 150, 157),
	    atlas.sub(151, 0, 150, 157),
	    atlas.sub(0, 0, 150, 157),
	    atlas.sub(302, 0, 150, 157),
	    ];
    super(walk[frame]);
    scale = 0.65;
    dir = x < 1371 / 2 ? -1 : 1; 
    scaleX = scaleX * dir * -1;
    this.x = x - 150 / 2 * scaleX;
    this.y = y - 157 * scaleY;
  }

  public override function update(et:Float) {
    if (!played && frame > 2) {
      hxd.Res.chick.play();
      played = true;
    }
    frameTime -= et;
    this.x += et * 250 * dir;
    if (frameTime <= 0) {
      frame++;
      if (frame >= walk.length) frame = 0;
      this.t = walk[frame];
      frameTime += 0.15;
    }
    if (x < -300 || x > 1500) {
      return false;
    }
    return super.update(et);
  }
}

class IntroChicken extends h2d.SpriteBatch.BatchElement {
  final walk: Array<h2d.Tile>;
  final jump: Array<h2d.Tile>;
  final atlas: h2d.Tile;
  final chickenBatch: h2d.SpriteBatch;
  final eggBatch: h2d.SpriteBatch;
  final score: Score;
  var dir = 1.0;
  var speed = 7;
  var frame = 0;
  var frameTime = 0.30;
  var hold = 1.0;
  var last = false;
  var jumping = 3;

  public function new(chickenBatch: h2d.SpriteBatch, eggBatch: h2d.SpriteBatch, score: Score, atlas: h2d.Tile) {
    this.chickenBatch = chickenBatch;
    this.eggBatch = eggBatch;
    this.score = score;
    this.atlas = atlas;

    walk = [
	    atlas.sub(0, 0, 150, 157),
	    atlas.sub(151, 0, 150, 157),
	    atlas.sub(0, 0, 150, 157),
	    atlas.sub(302, 0, 150, 157),
	    ];
    jump = [
	    atlas.sub(0, 157, 150, 157),
	    atlas.sub(150, 157, 149, 157),
	    ];
    super(walk[frame]);
    for (t in walk) {
      t.dx = -150 / 2;
      t.dy = -157 / 2;
    }
    for (t in jump) {
      t.dx = -150 / 2;
      t.dy = -157 / 2;
    }
    x = 1371 / 2;
    y = 771 / 2;

    hxd.Res.boot.play();
  }

  public override function update(et:Float) {
    frameTime -= et;
    if (frameTime <= 0) {
      frame++;
      if (jumping > 0) {
	if (frame >= jump.length) {
	  frame = 0;
	  jumping--;
	  this.y += 80;
	  if (jumping > 0) {
	    this.t = jump[frame];
	    frameTime -= 0.10;
	  } else {
	    this.t = walk[frame];
	  }
	} else {
	  frameTime -= 0.10;
	  this.y -= 80;
	  this.t = jump[frame];
	}
      } else {
	if (frame >= walk.length) frame = 0;
	this.t = walk[frame];
      }
      frameTime += 0.25;
    }
    if (jumping == 0) {
      if (scaleX <= 0) {
	if (hold > 0) {
	  scale = 0;
	  hold -= et;
	} else {
	  last = true;
	  scale = 0.05;
	  speed = 7;
	  dir = 1;
	}
      } else {
	scale = scaleX + speed * et * dir;
	if (scaleX > 3) {
	  dir = -1;
	  speed = 10;
	}
	if (last || speed >= 10) {
	  rotation += -20 * et * dir;
	}
	if (last && dir < 0 && scaleX <= 1) {
	  setup();
	  return false;
	}
      }
    }
    return super.update(et);
  }

  private function setup() {
    final laySound = hxd.Res.lay;
    final hatchSound = hxd.Res.hatch;

    final chicken = new MrsChicken(atlas);
    chicken.onEggLaid = (x, y) -> {
      laySound.play();
      final egg = new Egg(x, y, atlas);
      egg.onNewChick = (x, y) -> {
	hatchSound.play();
	final chick = new Chick(x, y, atlas);
	eggBatch.add(chick);
      }
      eggBatch.add(egg);
      score.newEgg();
    };
    chickenBatch.add(chicken);
  }
}

class IntroStar extends h2d.SpriteBatch.BatchElement {
  var dir = 1.0;

  public function new(x: Float, y: Float, atlas: h2d.Tile) {
    super(atlas.sub(286, 315, 217, 190));
    t.dx = -217 / 2;
    t.dy = -190 / 2;
    this.x = x;
    this.y = y;
    scale = 0.05;
  }

  public override function update(et:Float) {
    scale = scaleX + 4 * et * dir;
    if (scaleX > 5.02) {
      dir = -1.0;
    }
    rotation += 3 * et * dir;
    return scaleX > 0;
  }
}


class Main extends hxd.App {
  var chickenBatch: h2d.SpriteBatch;
  var eggBatch: h2d.SpriteBatch;

  override function init() {
    engine.backgroundColor = 0x08bde9;

    s2d.scaleMode = LetterBox(1371, 771);

    final atlas = hxd.Res.atlas.toTile();
    eggBatch = new h2d.SpriteBatch(atlas, s2d);
    eggBatch.hasRotationScale = true;
    eggBatch.hasUpdate = true;
    
    chickenBatch = new h2d.SpriteBatch(atlas, s2d);
    chickenBatch.hasUpdate = true;

    var score = new Score(atlas, s2d);
    
    eggBatch.add(new IntroStar(1371 / 2, 771 / 2, atlas));
    eggBatch.add(new IntroChicken(chickenBatch, eggBatch, score, atlas));
  }

  public static function main() {
#if js
    hxd.Res.initEmbed();
#else
    hxd.Res.initLocal();
#end
    new Main();
  }
}
